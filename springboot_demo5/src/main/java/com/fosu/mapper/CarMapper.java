package com.fosu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fosu.pojo.Car;

public interface CarMapper extends BaseMapper<Car> {

}
