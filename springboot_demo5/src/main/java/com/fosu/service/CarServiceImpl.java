package com.fosu.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fosu.mapper.CarMapper;
import com.fosu.pojo.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService{

    @Autowired(required = false)
    private CarMapper carMapper;

    @Override
    public List<Car> findAll() {
        return carMapper.selectList(null);
    }

    @Override
    public Car findById() {
        return carMapper.selectById(2);
    }

    @Override
    public List<Car> findAll2() {

        QueryWrapper<Car> queryWrapper = new QueryWrapper();
        queryWrapper.gt("price",900);
        return carMapper.selectList(queryWrapper);
    }

//    [1,"张三",{"id":100,"name":"李四","hobby":["敲代码","打游戏",{"gender":"女"}]}]





}

