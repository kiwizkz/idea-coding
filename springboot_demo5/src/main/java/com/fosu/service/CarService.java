package com.fosu.service;

import com.fosu.pojo.Car;

import java.util.List;

public interface CarService {

    List<Car> findAll();

    Car findById();

    List<Car> findAll2();
}
