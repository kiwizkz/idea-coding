package com.fosu.controller;

import com.fosu.pojo.Car;
import com.fosu.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("car")
@CrossOrigin
public class CarController {

  @Autowired
    private CarService carService;

@RequestMapping("find")
    public List<Car> findAll(){
        return carService.findAll();
    }
@RequestMapping("findById")
    public Car findById(){
    return carService.findById();
    }
    @RequestMapping("findAll2")
    public List<Car> findAll2(){
    return carService.findAll2();
    }

}
