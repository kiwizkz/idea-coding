package fosu;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("fosu.fo.mapper")
public class JT_SpringRun {
    public static void main(String[] args) {
        SpringApplication.run(JT_SpringRun.class, args);

    }
}
