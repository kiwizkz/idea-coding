package fosu.fo.controller;

import fosu.fo.pojo.Rights;
import fosu.fo.service.RightsService;
import fosu.fo.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/rights")


public class RightController {
    @Autowired
    private RightsService rightsService;

@GetMapping("getRightsList")
    public SysResult getRightsList(){
        List<Rights> rightsList= rightsService.getRightsList();
        return SysResult.success(rightsList);
    }




}
