package fosu.fo.controller;

import fosu.fo.pojo.User;
import fosu.fo.service.UserService;
import fosu.fo.vo.PageResult;
import fosu.fo.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("findall")
    public List<User> findall(){
        return userService.findAll();
    }

    @PostMapping("/login")
    public SysResult login(@RequestBody User user){
        String token=userService.login(user);
        if(StringUtils.hasLength(token)){
            return SysResult.success(token);
        }
        return SysResult.fail();
    }

//    请求路径: /user/list
//    请求类型: GET
//    请求参数: 后台使用PageResult对象接收
//    请求案例: http://localhost:8091/user/list?query=查询关键字&pageNum=1&pageSize=10
@GetMapping ("/list")
    public SysResult getUserList(PageResult pageResult){
        pageResult = userService.getUserList(pageResult);
        return SysResult.success(pageResult);
    }






}
