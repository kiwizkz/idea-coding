package fosu.fo.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import fosu.fo.mapper.UserMapper;
import fosu.fo.pojo.User;
import fosu.fo.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

    @Autowired(required = false)
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }

    @Override
    public String login(User user) {
        String password=user.getPassword();
        String md5Str= DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md5Str);
        //根据对象中不为空的属性充当where条件
        QueryWrapper<User> queryWrapper = new QueryWrapper(user);
        User userDB=userMapper.selectOne(queryWrapper);
        String uuid= UUID.randomUUID().toString()
                .replace("-", "");
        return userDB==null?null:uuid;
    }

    @Override
    public PageResult getUserList(PageResult pageResult) {


        long total = userMapper.selectCount(null);
        int pageNum = pageResult.getPageNum();
        int pageSize = pageResult.getPageSize();
        int startIndex = (pageNum-1) * pageSize;
        List<User> rows= userMapper.getUserList(startIndex,pageSize);
        pageResult.setTotal(total).setRows(rows);
        return pageResult;


    }
}
