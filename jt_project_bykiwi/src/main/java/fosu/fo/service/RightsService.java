package fosu.fo.service;

import fosu.fo.pojo.Rights;

import java.util.List;

public interface RightsService {

    List<Rights> getRightsList();
}
