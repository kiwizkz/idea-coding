package fosu.fo.service;

import fosu.fo.pojo.User;
import fosu.fo.vo.PageResult;

import java.util.List;

public interface UserService  {

     List<User> findAll();

     String login(User user);

    PageResult getUserList(PageResult pageResult);
}
