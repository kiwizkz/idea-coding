package fosu.fo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fosu.fo.pojo.Rights;
import org.springframework.stereotype.Repository;

@Repository
public interface RightsMapper extends BaseMapper<Rights> {

}
