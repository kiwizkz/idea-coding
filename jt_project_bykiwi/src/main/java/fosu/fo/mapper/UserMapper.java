package fosu.fo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fosu.fo.pojo.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper extends BaseMapper<User>{

    List<User> getUserList(int startIndex, int pageSize);
}
